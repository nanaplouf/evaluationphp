<?php
$title = "Home";

require_once(__DIR__ . "/partials/head.php");
?>
<h1 class='text-center my-5'> Welcome in Plante&co</h1>

<h2 class='text-center lead'>Last products</h2>
<div class="row p-5">
    <?php
    foreach ($products as $product) {
    ?>
        <div class="col-md-4  mb-2 ">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $product['title']; ?></h5>
                    <p class="card-text"><?php echo $product['description']; ?></p>
                    <p class="card-text"><?php echo $product['price']; ?>€</p>
                    <?php
                    if (isset($_SESSION['user']) && $_SESSION['user']['admin'] == true) {
                    ?>
                        <a href="/product-read?id=<?php echo $product['id']; ?>" class="btn btn-sm btn-primary">View Product</a>
                        <a href="/product-update?id=<?php echo $product['id']; ?>" class="btn  btn-sm btn-warning">Update product</a>
                        <a href="/product-delete?id=<?php echo $product['id']; ?>" class="btn  btn-sm btn-danger">Delete</a>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>

<?php
require_once(__DIR__ . "/partials/footer.php");
?>