<?php
$title = 'Create products';
require_once(__DIR__ . "/../partials/head.php") ?>






<h1>creation produit</h1>
<form action="/product-create" method="POST">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input type="number" class="form-control" name="price" id="price">
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
</form>







<?php require_once(__DIR__ . "/../partials/footer.php") ?>