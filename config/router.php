<?php

require_once(__DIR__ . "/function.php");
require_once(__DIR__ . '/db.php');

$uri = parse_url($_SERVER['REQUEST_URI'])['path'];

$routes = [
    '/' => 'HomeController.php',
    '/cgv' => 'CgvController.php',
    // connexion inscription
    '/login' => 'LoginController.php',
    '/logout' => 'LogoutController.php',
    '/register' => 'RegisterController.php',
    // admin
    '/admin' => 'Admin',
    '/admin-update' => 'AdminUpdateController.php',
    '/admin-delete' => "AdminDeleteController.php",
    // crud product
    '/product' => 'ProductController.php',
    '/product-create' => 'ProductCreateController.php',
    '/product-read' => 'ProductReadController.php',
    '/product-update' => 'ProductUpdateController',
    '/product-delete' => 'ProductDeleteController.php',



];

$securedRoutes = [
    '/admin' => 'AdminController.php',
    '/admin-update' => 'AdminUpdateController.php',
    '/admin-delete' => "AdminDeleteController.php",
    '/product-update' => 'ProductUpdateController.php',
    '/product-delete' => 'ProductDeleteController.php',
    '/product-create' => 'ProductCreateController.php',
];

if (array_key_exists($uri, $securedRoutes)) {
    if (!isset($_SESSION['user']) || !$_SESSION['user']['admin'] == 1) {
        redirectToRoute('/login');
    }
}

if (array_key_exists($uri, $routes)) {
    require_once(__DIR__ . "/../app/Controllers/" . $routes[$uri]);
} else {
    http_response_code(404);
    require_once(__DIR__ . '/../app/Controllers/404.php');
}
