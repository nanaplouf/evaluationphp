<?php
$title = "login";

require_once(__DIR__ . "/../partials/head.php");
?>

<div class="container my-5 bg-dark text-light p-5 w-25">
    <h1 class='text-center text-light'>Login</h1>
    <div class="row justify-content-center ">

        <div class="w-100">
            <form action="" method="POST">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                <a class=" ml-2 text-light" href="/register"> No account? create one</a>
            </form>
            <?php if (isset($error)) {
                echo "<p class='text-danger'>" . $error . "<p>";
            } ?>
        </div>
    </div>
</div>

<?php
require_once(__DIR__ . "/../partials/footer.php");
?>