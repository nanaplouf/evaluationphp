<?php
$title = "produit";

require_once(__DIR__ . "/../partials/head.php");
?>





</div>
<h1 class='text-center my-3'>Product Page</h1>



<div class='d-flex justify-content-center my-4'>
    <a class="btn btn-primary w-75" href="/product-create">create product</a>
</div>


<div class="row p-5">
    <?php
    foreach ($products as $product) {
    ?>
        <div class="col-md-4  mb-2">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $product['title']; ?></h5>
                    <p class="card-text"><?php echo $product['description']; ?></p>
                    <p class="card-text"><?php echo $product['price']; ?>€</p>
                    <?php
                    if (isset($_SESSION['user']) && $_SESSION['user']['admin'] == true) {
                    ?>
                        <a href="/product-read?id=<?php echo $product['id']; ?>" class="btn btn-sm btn-primary">View Product</a>
                        <a href="/product-update?id=<?php echo $product['id']; ?>" class="btn  btn-sm btn-warning">Update product</a>
                        <a href="/product-delete?id=<?php echo $product['id']; ?>" class="btn  btn-sm btn-danger">Delete</a>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>

<?php
require_once(__DIR__ . "/../partials/footer.php");
?>