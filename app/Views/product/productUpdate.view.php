<?php
$title = "Home";

require_once(__DIR__ . "/../partials/head.php");
?>



<div class="mx-auto" style="width: 400px;">

    <form action="" method="POST">
        <h1>Update product</h1>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="<?php echo $product['title'] ?>">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" rows="5"><?php echo $product['description'] ?></textarea>
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="number" class="form-control" name="price" value="<?php echo $product['price'] ?>">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    <a class='btn btn-dark mt-2' href="/product">Go back to product</a>
</div>

<?php
require_once(__DIR__ . "/../partials/footer.php");
?>