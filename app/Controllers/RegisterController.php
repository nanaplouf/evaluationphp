<?php






// on verifie que le formulaire a ete envoyé
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password'])) {
    // on met les information du formulaire dans des variable
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    // ----------------------------///

    // verification de la validité de l'email puis de la presence de l'email en bdd
    if (!filter_var($email, FILTER_VALIDATE)) {
        $error = "Invalid Email";
        require_once(__DIR__ . '/../Views/security/register.view.php');
        exit;
    }

    // je recupere mon utilisateur en base de donnee et je les met dans la variable user
    $userQuery = "SELECT * FROM user WHERE email = :email";
    $userStatement = $mysqlClient->prepare($userQuery);
    $userStatement->bindParam(':email', $email);
    $userStatement->execute();
    // quand l'element est unique on utilise fetch et non fetchAll
    $user = $userStatement->fetch();

    // -----------------------------//

    if ($user) {
        $error = "Email already exist";
    } else {
        $passwordValid = preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/", $password);

        if ($passwordValid) {

            $userQuery = "INSERT INTO user (name,email,password) VALUES (:name, :email, :password)";
            // je prepare ma requete sql a l'envoie
            $userStatement = $mysqlClient->prepare($userQuery);
            //  je modifie les valeurs de ma requete en fonction des valeurs du formulaire
            $userStatement->bindParam(':name', $name);
            $userStatement->bindParam(':email', $email);
            $userStatement->bindParam(':email', $password);
            // j'execute la requete
            $userStatement->executed();
            // je redirige l'utilisateur vers la page user pour qu'il vois le nouveau produit 
            redirectToRoute('/login');
        } else {
            $error = "
            - at least 8 characters <br>
            - at least one character in uppercase <br>
            - at lest one character in lowercase<br>
            - at least one digit<br>
            - at least one special character<br>
            ";
        }
    }
}

require_once(__DIR__ . '/../Views/security/register.view.php');
